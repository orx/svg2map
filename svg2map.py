#!/usr/bin/python

# svg2map.py
#
# Converts SVG game maps to ORX config files and PNG graphics
#
# Copyright (C) 2012 Eric Poulsen <eric@zyxod.com>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# system imports
import sys
import os
import optparse
import rsvg
import tempfile
import cairo
import re
from uuid import uuid4
from cStringIO import StringIO
from lxml import etree
from PIL import Image, ImageDraw
#from optparse import OptionParser
import argparse
from OrderedDict import OrderedDict

#local imports
from euclid import Point2, Vector2, Matrix3

progname  = 'svg2map'
copyright = '2012'
author    = 'Eric Poulsen <eric@zyxod.com>'
options   = None # Globally set by main()


class UnsupportedPathException(Exception):
    pass

class IncompatibleTransformException(Exception):
    pass

class IncompatibleShapeException(Exception):
    pass

dictType = OrderedDict        

class OrxConfigSection(object):
    __slots__ = ['name', 'kv', 'comments']

    indent = '  '
    endLine = '\n'

    def __init__(self, name, kv, comments = []):
        self.name = name
        self.kv = kv
        self.comments = comments

    def __str__(self):
        out = ''
        oneComment = len(self.comments) == 1
        if oneComment:
            out += '[%s] ; %s%s' % (self.name, self.comments[0], OrxConfigSection.endLine)
        else:
            out += '[%s]%s' % (self.name, OrxConfigSection.endLine)

        for k,v in self.kv.items():
            out += '%s%s = %s%s' % (self.__class__.indent, str(k), str(v), self.__class__.endLine)

        if not oneComment:
            for comment in self.comments:
                out += '%s; %s%s' % (self.__class__.indent, comment, self.__class__.endLine)

        return out


class OrxPhysicsObject(object):

    ''' Base class for ORX physics objects. Known subclasses are OrxMesh and OrxSphere '''


    # These values are assumed by ORX
    # if a key is not explicitly set

    orxDefaults = dictType([
        #('CheckMask'  , '0xFFFF'),
        #('SelfFlags'  , '0x0001'),
        ('Friction'   , '0'),
        ('Restitution', '0'),
        ('Density'    , '0'),
        ('Solid'      , 'false')
        ])

    # Class defaults, can be 
    # overridden by SVG element
    defaults = dictType([
        ('CheckMask'  , '0xFFFF'),
        ('SelfFlags'  , '0x8000'),
        ('Solid'      , 'true')
        ])
    
    __slots__ = ['sectionName', 'comments', 'kv']

    def __init__(self, element, kv):

        # Key/values from the SVG element
        svgKv = dict(((k[4:], v) for k,v in element.nsAttrib.items() if k.startswith('orx-')))
        
        defaultSectionName = 'bodyPart' + (element['id'] or OrxPhysicsObject.genUuid())
        self.sectionName = svgKv.pop('section-name', defaultSectionName)

        self.kv = dictType()

        for key, value in self.__class__.defaults.items() + kv.items() + svgKv.items():
            if self.sectionName == 'WaterSensor':
                print key, value
            if value != self.__class__.orxDefaults.get(key):
                self.kv[key] = value or ''
            else:
                del self.kv[key]

        self.comments = []

    @staticmethod
    def genUuid():
        return str(uuid4())
        
    @staticmethod
    def applyTransforms(points, matrices):
        # Multiply each point by all transforms
        # Matrix * point = point
        # Point * matrix is invalid
        # Hence 'lambda a, b: b * a' reversal below
        path = [ Point2(*p) for p in points ]
        path = [ reduce(lambda a, b: b * a, [p] + matrices) for p in path ]
        return path

    def validateTransforms(self, matrices):
        raise NotImplementedError()

    def validateShape(self):
        raise NotImplementedError()

    def toOrxConfig(self):
        raise NotImplementedError()

    @property
    def bodyPartSectionName(self):
        return self.sectionName

    @property
    def center(self):
        raise NotImplementedError()

    @staticmethod
    def aabb(points, start = None):
        if start is None:
            xMin = float('inf')
            xMax = float('-inf')
            yMin = float('inf')
            yMax = float('-inf')
        else:
            xMin, yMin = start[0]
            xMax, yMax = start[1]

        for p in points:
            xMin = min(xMin, p[0])
            xMax = max(xMax, p[0])
            yMin = min(yMin, p[1])
            yMax = max(yMax, p[1])
        return ((xMin, yMin), (xMax, yMax))


class OrxMesh(OrxPhysicsObject):
    __slots__ = ['points', '_centroid', '_aabb']

    def __init__ (self, element, points, kv = {}):
        super(OrxMesh, self).__init__(element, kv)
        self.points = list(points)
        self._centroid = None
        self._aabb = None

    def __str__(self):
        return '<OrxMesh ' + '  '.join('%.1f,%.1f' % x for x in self.points) + '>'

    def validateTransforms(self, matrices):
        return True, ''

    def applyTransforms(self, matrices):
        self.points = [ tuple(x) for x in OrxPhysicsObject.applyTransforms(self.points, matrices) ]

    @property
    def aabb(self):
        if self._aabb:
            return self._aabb
        self._aabb = OrxPhysicsObject.aabb(self.points)
        return self._aabb

    @property
    def center(self):
        if self._centroid:
            return self._centroid

        centroid = Point2(0, 0)
        area = 0.0

        pcount = len(self.points)
        
        for i in range(0, pcount):
            # last set will be points[-1], points[0]
            p1, p2 = [ Point2(*x) for x in (self.points[i], self.points[(i+1) % pcount])]
            a = p1.x * p2.y - p2.x * p1.y           
            area += a
            centroid.x += (p1.x + p2.x) * a
            centroid.y += (p1.y + p2.y) * a

        area *= 0.5
        centroid[0] /= (6.0 * area)
        centroid[1] /= (6.0 * area)
        self._centroid = centroid
        return self._centroid

    def validateShape(self):
        if len(self.points) > 8:
            return False, 'OrxMesh has too many points: %d (%d max)' % (len(self.points), 8)
        clockwise, convex = polyCheck(self.points)

        if not convex:
            return False, 'OrxMesh is not convex'

        if not clockwise: # This one is fixable
            self.points.reverse()

        return True, ''


    def toOrxConfig(self):
        kv = dictType([
                ('Type', 'mesh'), 
                ('VertexList', ' # '.join(('{%f, %f, 0.0}' % p for p in self.points))),
             ] + [ (k,v) for k,v in self.kv.items() ])

        return OrxConfigSection(self.bodyPartSectionName, kv , self.comments)


class OrxSphere(OrxPhysicsObject):
    __slots__ = ['center', 'radius']

    def __init__ (self, element, center, radius, kv = {}):
        super(OrxSphere, self).__init__(element, kv)
        self.center = center
        self.radius = radius

    def applyTransforms(self, matrices):
        points = [self.center, (self.center[0] + self.radius, self.center[1])]
        points = OrxPhysicsObject.applyTransforms(points, matrices)
        self.center = tuple(points[0])
        self.radius = abs(points[0] - points[1])
        
    def validateTransforms(self, matrices):
        # Use a simple square mesh to ensure 
        # the transform won't shear or scale
        # the OrxSphere nonuniformly
        points = ((0,0), (0,1), (1,1), (1,0))
        points = OrxPhysicsObject.applyTransforms(points, matrices)

        # Still a square?
        ok = isSquare(*points)
        if not ok:
            return False, 'Transforms would deform object in a way not compatible with physics engine'
        else:
            return True, ''

    @property
    def aabb(self):
        return (
                (self.center[0] - self.radius, self.center[1] - self.radius), 
                (self.center[0] + self.radius, self.center[1] + self.radius)
               )
    def validateShape(self):
        if self.radius != 0:
            return True, ''
        else:
            return False, 'OrxSphere radius is zero'

    def __str__(self):
        return '<OrxSphere %.1f, %.1f r: %.1f>' % (self.center[0], self.center[1], self.radius)

    def toOrxConfig(self):

        kv = dictType([
                ('Type', 'sphere'),
                ('Center', '{%f, %f, 0.0}' % self.center),
                ('Radius', str(self.radius)),
             ] + [ (k,v) for k,v in self.kv.items() ] )

        return OrxConfigSection(self.bodyPartSectionName, kv, self.comments)

class SvgParser(object):

    ''' lxml has a funky way of dealing with namespaces.  If you declare 
        a namespace such as:
        <rootelem xmlns:mango="http://mango.com/mango" />

        Then an attribute such as:

        <somelement mango:kumquat = "Pommelo" /> 

        will become:

        <somelement {http://mango.com/mango}kumquat = "Pommelo" /> in the parsed tree.

        The reason for this is to prevent namespace ambiguity, 
        since namespaces can be declared at any level (not just root)
        and a lower-in-the-tree namespace will "hide" a higher 
        namespace.

        This class is designed to help with this world, since our
        real-world problem isn't likely to have a namespace clash.

        This class will generate a per-parse instance of a custom
        XML element class that is aware of the document namespaces,
        and allows them to be accessed in a programmer-friendly way.

        Additionally, it returns an lxml.tree object wrapped around
        a custom class so that the xpath method can be called without
        passing the 'namespaces' parameter every single time.

        Since this script parses inkscape svg documents
        that have all the xmlns declarations in the root
        of the document, this works for me.
    '''

    @classmethod
    def parse(cls, fileName):

        SvgElement = cls.generateElementClass()
        XMLParser = etree.XMLParser

        with open(fileName, 'r') as fileHandle: 
            parser = XMLParser()
            parser_lookup = etree.ElementDefaultClassLookup(element = SvgElement)
            parser.set_element_class_lookup(parser_lookup)
            tree = etree.parse(fileHandle, parser)
        ns = dict([x for x in  tree.getroot().nsmap.items() if not x[0] is None ])
        SvgElement.setNamespace(ns)

        return cls.__TreeWrapper(tree, ns)

    class __TreeWrapper(object):

        def __init__(self, tree, ns):
            self.__tree = tree
            self.__ns = ns

        def xpath(self, *args, **kwargs):
            kwargs['namespaces'] = self.__ns
            return self.__tree.xpath(*args, **kwargs)

        def __getattr__(self, name):
            if name == 'xpath':
                return self.xpath
            return getattr(self.__tree, name, None)

    @classmethod
    def generateElementClass(self):
        ''' We generate classes so that each class can have it's own class-level namespace '''
        return type('SvgElement', (self.__SvgElement,), {})

    class __SvgElement(etree.ElementBase):
        
        namespace = {}
        rev_namespace = {}


        @classmethod
        def convertAttribName(self, name):
            if ':' in name:
                first, second = name.split(':')
                if first in self.namespace:
                    name = '{%s}%s' % (self.namespace[first], second)
            return name

        @classmethod
        def setNamespace(self, ns):
            self.namespace = ns
            self.rev_namespace = {v:k for k,v in ns.items()}

        def __getitem__ (self, key):
            return self.get(key)

        def __setitem__ (self, key, item):
            self.attrib[self.convertAttribName(key)] = item

        def get(self, key, default = None):
            return self.attrib.get(self.convertAttribName(key), None)

        def getStyle(self, key, default = None):
            style = self.unpackStyle()
            return style.get(key, default)

        def setStyle(self, key, value):
            style = self.unpackStyle()
            if value is None:
                del style[key]
            else:
                style[key] = value
            self.packStyle(style)

        def unpackStyle(self):
            pairs = [ x.strip() for x in self['style'].split(';') ]
            return dict([ [ y.strip() for y in x.split(':') ] for x in pairs ])

        def packStyle(self, styleDict):
            self['style'] = ';'.join([':'.join(x) for x in styleDict.items()])

        @property
        def nsAttrib(self):
            ''' Returns a dict of attributes adjusted for the lxml namespace quirs '''
            return dict((self.convertAttribName(key), value) for key, value in self.attrib.items())
            

        @staticmethod
        def pointAdd (p1, p2):
            result = list(p1)
            result[0] += p2[0];
            result[1] += p2[1]
            return result
        
        def pathPhysics(self, kv = {}):
            args = []
            path = []
            objects = []
            zero = (0,0)
            current = [0, 0]
            command = None
            relative = False
            argStack = [] # list of tuples of args
            # See http://www.w3.org/TR/SVG/paths.html
            argCount = { 'm': 2, 'l': 2, 'h': 1, 'v': 1, 'a': 7, 'c': 6, 's': 4, 'q': 4, 't': 2, 'z': 0}

            def addObject(o):
                objects.append(o)
                cnt = len(objects)
                if cnt > 1:
                    o.sectionName += ('.' + str(cnt - 1))

            for val in re.findall(r'(?:[a-zA-Z]+|[-\d.]+)', self['d']): 
                if re.match (r'[a-zA-Z]', val):
                    relative = val == val.lower()
                    command = val.lower()
                    argStack = []
                else:
                    args.append(float(val))
                    if len(args) == argCount[command]:
                        argStack.append(tuple(args))
                        if command in ('m', 'l'): # Move or lineto
                            if command == 'm' and len(path) > 2: # Terminate any existing path
                                addObject(OrxMesh(self, path, kv))
                                path = []
                            #current = SvgElement.pointAdd(args, current if relative else zero)
                            current = self.__class__.pointAdd(args, current if relative else zero)
                            path.append(tuple(current))
                            command = 'l' # Further points are implicit lineto
                        elif command == 'a':  # Arc
                            #
                            # ### ARC
                            #

                            rx, ry, xAxisRot, largeArc, sweep, x, y = args
                            if len(path) > 1: # has last moveto
                                raise UnsupportedPathException('Cannot use arc as part of larger path for physics')
                            if rx != ry:
                                raise UnsupportedPathException('Arc is not round; unequal radii')

                            radius = rx

                            if len(argStack) == 2:
                                # Consider this path:
                                # m 
                                #  182.85714,219.50504 
                                # a 
                                #  47,47 0 1 1 -94,0 
                                #  47,47 0 1 1  94,0 z
                                # 
                                # Args for arc are:
                                # radiusX, radiusY xAxisRot largeArcFlag sweepFlag finalX, finalY
                                # 
                                # Starting X/Y are inferred from "current" position.
                                # Hence below, 'start' of first arc is the variable current, 
                                # 'mid' is endpoint of first arc (mid is also start 
                                # of 2nd arc, since it's now 'current')
                                # and 'end' is the end point of the 2nd arc.
                                #

                                threshold = 1e-6

                                start = Point2(*current)
                                mid   = start + Vector2(*argStack[0][-2:])
                                end   = mid   + Vector2(*argStack[1][-2:])

                                if start != end:
                                    raise UnsupportedPathException('"Dual arc circle": arc1 begin != arc2 end')

                                diameter = (start - end).magnitude()

                                if diameter - (radius * 2) > threshold:
                                    raise UnsupportedPathException('"Dual arc circle": Diameter != radius * 2')

                                center = start + ((mid - start) / 2)
                                addObject(OrxSphere(self, center, radius, kv))
                                argStack = []

                                pass # check to see if forms a circle
                            elif len(argStack) > 2:
                                pass # This is a problem
                                
                        elif command == 'h': # Horizontal lineto
                            current[0] = current[0] + args[0] if relative else args[0]
                            path.append(tuple(current))
                        elif command == 'v': # Vertical lineto
                            current[1] = current[1] + args[0] if relative else args[0]
                            path.append(tuple(current))
                        elif command in ('c', 'b', 's'): # Bezier curves
                            raise UnsupportedPathException('Bezier curves are not supported')
                        elif command == 'z':
                            pass # paths are assumed closed for physics purposes.
                            
                        args = []

            if len(path) > 2: # Cleanup dangling objects

                addObject(OrxMesh(self, path, kv))

            return objects

        def rectPhysics(self, kv):
            """ Does not deal w/rounded corners """
            x = float(self['x'])
            y = float(self['y'])
            w = float(self['width'])
            h = float(self['height'])
            rx = float(self['rx'] or 0)
            ry = float(self['ry'] or 0)
            if rx or ry:
                raise UnsupportedPathException("Rounded corners not supported for <rect>")

            mesh = OrxMesh(self, ((x, y), (x + w, y), (x + w, y + h), (x, y + h)), kv)
            return [mesh]

        def getPhysics(self, kv):
            tag = self.tagName.split(':')[-1]
            funcName = tag + 'Physics'
            try:
                func = getattr(self,funcName)
            except AttributeError:
                raise RuntimeError('Need implementation of method %s' % (funcName,))

            return func(kv)
            
        def getMatrices(self):
            transforms = []
            cur = self
            while cur is not None:
                transform = cur['transform']
                if transform:
                    transforms.append(transform)
                cur = cur.getparent()
            return sum([self.__class__.transformToMatrices(x) for x in transforms], [])

        @staticmethod
        def transformToMatrices(transform):
            """Parses the SVG "transform" attribute into an array of euclid matrices"""

            matrices = []
            
            # split out transformation commands from string
            for match in re.finditer(r'(matrix|translate|scale|rotate|skewx|skewy)\s*\(([^\)]+)\)', transform):
                command = match.group(1).lower()
                params = map(float, re.split(r'\s*,\s*', match.group(2)))

                if command == 'matrix':
                    # SVG Matrix parameters (A,B,C,D,E,F):
                    # | A C E |
                    # | B D F |
                    # | 0 0 1 |
                    #
                    # euclid.Matrix3 matrix locations:
                    # | A B C |
                    # | E F G |
                    # | I J K |
                    
                    #
                    matrix = Matrix3()
                    matrix.a, matrix.e, matrix.b = params[0:3]
                    matrix.f, matrix.c, matrix.g = params[3:6]
                    #matrix[0:6] = params[0:6]
                    matrices.append(matrix)
                elif command == 'translate':
                    params += [0.0, 0.0]
                    matrices.append(Matrix3.new_translate(*params[0:2]))
                elif command == 'scale':
                    params += [1.0, 1.0]
                    matrices.append(Matrix3.new_scale(*params[0:2]))
                elif command == 'rotate':
                    params += [0.0, 0.0, 0.0]
                    angle = params[0]
                    xy = params[1:3]
                    matrices.append(Matrix3.new_translate(*xy))
                    matrices.append(Matrix3.new_rotate(math.radians(angle)))
                    matrices.append(Matrix3.new_translate(*map(lambda q:-q, xy)))
                elif command == 'skewx':
                    params += [0.0]
                    matrix = Matrix3()
                    matrix[3] = math.tan(math.radians(params[0]))
                    matrices.append(matrix)
                elif command == 'skewy':
                    params += [0.0]
                    matrix = Matrix3()
                    matrix[1] = math.tan(math.radians(params[0]))
                    matrices.append(matrix)
                return matrices

        @property
        def tagName(self):
            tag = self.tag
            if tag.startswith('{'):
                ns, tag = tag[1:].split('}')
                return self.rev_namespace[ns]+':'+tag
            return tag



def isSquare(a, b, c, d):
    return isParallelogram(a, b, c, d, True, True)

def isRectangle(a, b, c, d):
    return isParallelogram(a, b, c, d, True)

def isParallelogram(a, b, c, d, checkForRectangle = False, checkForSquare = False, threshold = 1e-6):
    """ Points must be in order around perimeter """

    if checkForSquare:
        checkForRectangle = True
    # Assuming this sort of parallelogram:
    #
    # a-----b
    # |     |
    # |     |
    # |     |
    # d-----c
    #
    # Rotation doesn't matter.

    a, b, c, d = [ Vector2(*q) for q in (a, b, c, d) ]

    ab = (a - b).magnitude_squared()
    bc = (b - c).magnitude_squared()
    cd = (c - d).magnitude_squared()
    da = (d - a).magnitude_squared()

    # Ensure opposite sides are same length
    abEqualsCd = abs(ab - cd) < threshold
    bcEqualsDa = abs(bc - da) < threshold

    isParallelogram = abEqualsCd and bcEqualsDa

    if not isParallelogram or not checkForRectangle:
        return isParallelogram

    # If diagonals are the same, it's a rectangle
    bd = (b - d).magnitude_squared()
    ac = (a - c).magnitude_squared()
    isRectangle = abs(bd - ac) < threshold

    if not isRectangle or not checkForSquare:
        return isRectangle

    bcEqualsCd = abs(bc - cd) < threshold

    isSquare = bcEqualsCd

    return isSquare

        
def polyCheck(points):
    """ Returns a tuple of (boolean, boolean) for clockwise, convex """
    ttl = 0
    pointCount = len(points)
    if pointCount < 3:
       return None, None

    posCount = 0
    negCount = 0

    for i in range(pointCount):
        j = (i + 1) % pointCount
        k = (i + 2) % pointCount
        pj = points[j]
        pk = points[k]
        pi = points[i]
        z  = (pj[0] - pi[0]) * (pk[1] - pj[1])
        z -= (pj[1] - pi[1]) * (pk[0] - pj[0])

        if   z > 0: posCount += 1
        elif z < 0: negCount += 1

    clockwise = posCount > negCount
    convex = (posCount == pointCount) or (negCount == pointCount)

    return clockwise, convex


class SvgToMap(object):

    def __init__(self, fileName, outDir, bodyPartDefaults = {}, bodyDefaults = {}, objectDefaults = {}, metaDefaults = {}, graphicDefaults = {}, debug = False):

        self.fileName = fileName
        self.outDir = outDir
        self.bodyPartDefaults = bodyPartDefaults
        self.bodyDefaults = bodyDefaults
        self.objectDefaults = objectDefaults
        self.metaDefaults = metaDefaults
        self.graphicDefaults = graphicDefaults
        self.debug = debug

        self.physicsLayerIndex = -1

        self.tree = SvgParser.parse(fileName)
        self.findSvgLayerNodes()
        self.rsvg = rsvg.Handle(fileName)

        self.size = (self.rsvg.get_property('width'), self.rsvg.get_property('height'))


    class ImageInfo(object):
        '''Holds information about layer images'''

        def __init__(self, name, fileName, bbox, orxParams):
            self.name = name
            self.fileName = fileName
            self.bbox = bbox
            self.orxParams = orxParams


    class LayerInfo(object):
        '''Thin wrapper around SvgElement, with layer-specific hooks'''
        def __init__(self, node):
            self.node = node
            self.originalAttr = node.nsAttrib

        def __getitem__(self, name):
            return self.node.__getitem__(name)

        def __getattr__(self, name):
            if name == 'isPhysics':
                return self.node['inkscape:label'].lower() == 'physics'
            elif name == 'name':
                return self.node['inkscape:label']
            return getattr(self.node, name)

    
    def toPng(self, outFile, width=None, height=None, nodeId = None):

        svgWidth, svgHeight = self.size
        
        if not width and not height:
            x_scale = y_scale = 1
        elif width and not height:
            x_scale = y_scale = width / float(svgWidth)
        elif height and not width: 
            x_scale = y_scale = height / float(svgHeight)
        else:
            x_scale = width / float(svgWidth)
            y_scale = height / float(svgHeight)

        height = y_scale * svgHeight
        width = x_scale * svgWidth

        svgsurface = cairo.SVGSurface (None, width, height)
        svgctx = cairo.Context(svgsurface)
        svgctx.scale(x_scale, y_scale)

        if nodeId is not None:
            # For some reason, rsvg requires a '#' before the node id.
            self.rsvg.render_cairo(svgctx, id = '#' + nodeId) 
        else:
            self.rsvg.render_cairo(svgctx)
        
        svgsurface.write_to_png(outFile)

    def findSvgLayerNodes(self):
        self.layers = []
        for node in self.tree.xpath('//svg:g[@inkscape:groupmode="layer"]'):
            layerInfo = self.LayerInfo(node)
            print 'layer', layerInfo.name
            self.layers.append(layerInfo)

    def extractImages(self):
        '''Creates a cropped image for each layer into outDir.  
           Returns a list of 3-tuple of (image_file_name, bounding_box, orx_params).
           Bounding box is relative to overall size of SVG.'''

        ret = []

        index = 0

        for layer in self.layers:
            if layer.isPhysics or layer.getStyle('display').lower() == 'none':
                self.physicsLayerIndex = index
                continue

            outFilename = os.path.join(self.outDir, layer.name + '.png')
            print layer.name, layer.getStyle('display'), layer['id']
            self.toPng(outFilename, nodeId = layer['id'])
            img = Image.open(outFilename)
            bbox = img.getbbox()
            if bbox is None: # Empty image
                os.remove(outFilename)
                continue
            orxParams = dict([(k[4:], v) for k, v in layer.nsAttrib.items() if k.startswith('orx-')])
            img = img.crop(bbox)
            img.save(outFilename)
            ret.append(self.ImageInfo(layer.name, outFilename, bbox, orxParams))
            index += 1

        return ret



    def extractPhysics(self):
        '''Process an SVG file into physics objects.
            
           Returns a tuple of OrxPhysicsObject.

        '''

        physicsLayer = None
        cnt = 0
        for layer in self.layers:
            if layer.isPhysics:
                cnt += 1
                layer.setStyle('display', 'none')
                physicsLayer = layer

        if cnt > 1:
            raise RuntimeError('Found %d physics layers.  Max of 1 is allowed' % (cnt,))

        ## Get the nodes that live in the 
        ## physics layer
        phyObjs = []
        
        def complain(node, exc, reason):
            tagId = node['id']
            if tagId:
                reason += ' <%s id="%s"> @ [%s:%d]' % (node.tagName, tagId, self.fileName, node.sourceline)
            else:
                reason += ' <%s> @ [%s:%d]' % (node.tagName, self.fileName, node.sourceline)
            if isinstance(exc, Exception):
                raise exc.__class__(reason)
            else:
                raise exc(reason)

        nodes = physicsLayer.xpath('./*')
        print 'Found', len(nodes), 'physics nodes'

        aabb = None
        baseFileName = os.path.basename(self.fileName)
        for node in nodes:
            matrices = node.getMatrices()
            try:
                nodeObjs = node.getPhysics(self.bodyPartDefaults)  
            except (UnsupportedPathException, IncompatibleShapeException, IncompatibleTransformException) as e:
                complain(node, e, str(e))

            nodeId = node['id']
            for o in nodeObjs:
                #print 'id: %s center: %d,%d' % (nodeId, o.center[0], o.center[1])

                transformsOk, reason = o.validateTransforms(matrices)
                if not transformsOk:
                    complain(node, IncompatibleShapeException, reason)

                # Do this before, since transforms
                # may invalidate the shape
                o.applyTransforms(matrices)

                shapeOk, reason = o.validateShape()
                if not shapeOk:
                    complain(node, IncompatibleShapeException, reason)

                o.comments.append('src: %s:%d id="%s"' % (baseFileName, node.sourceline, nodeId))
                aabb = OrxPhysicsObject.aabb(o.aabb, aabb)
            phyObjs += nodeObjs

        print 'extents', aabb, 'w', abs(aabb[0][0] - aabb[1][0]), 'h', abs(aabb[0][1] - aabb[1][1])
        return phyObjs, aabb

    def convert(self, outDir):

        if not os.path.isdir(outDir):
            print 'Output directory \'%s\' does not exist' % (outDir,)
            sys.exit(1)

        baseName = os.path.basename(self.fileName)
        if baseName.lower().endswith('.svg'):
            baseName = baseName[:-4]

        configOutFile = os.path.join(outDir, baseName + '.ini')

        phyObjs, aabb = self.extractPhysics()
        images = self.extractImages()

        # Check for duplicate physics object names names
        names = set()
        for o in phyObjs:
            originalName = o.sectionName
            cnt = 0
            while o.sectionName in names:
                cnt += 1
                o.sectionName = originalName + '.' + str(cnt)
            if cnt:
                print "Warning: duplicate object '%s' renamed to '%s'" % (originalName, o.sectionName)
            names.add(o.sectionName)


        # Create physics debugging image, if necessary
        if self.debug:
            svgWidth, svgHeight = self.size
            debugImage = Image.new('RGBA',(svgWidth, svgHeight))
            draw = ImageDraw.Draw(debugImage)
            fileName = os.path.join(self.outDir, '_physics_debug.png')
            for o in phyObjs:
                if isinstance(o, OrxMesh):
                    points = o.points
                    draw.line(points + [points[0]], width = 1)
                elif isinstance(o, OrxSphere):
                    x,y = o.center
                    draw.ellipse((x - o.radius, y - o.radius, x + o.radius, y + o.radius))

            images.append(self.ImageInfo('_physics_debug', fileName, (0, 0), {}))
            debugImage.save(fileName)


        upperLeft = Point2(*[-q / 2 for q in self.size])

        with open(configOutFile, 'w') as configFile:
            print 'Writing:', configOutFile
            partNames = []
            for o in phyObjs:
                partNames.append(o.bodyPartSectionName)
                configData = o.toOrxConfig()
                configFile.write(str(configData) + OrxConfigSection.endLine)

            width  = abs(aabb[0][0] - aabb[1][0])
            height = abs(aabb[0][1] - aabb[1][1])

            objectNames = []

            zStep = 0.10

            z = 0 + zStep * self.physicsLayerIndex

            print 'physicsLayerIndex', self.physicsLayerIndex


            for index, img in enumerate(images):
                print index, img.name
                if index == self.physicsLayerIndex:
                    z = 0 - zStep

                objName     = 'object.%s' % (img.name,)
                position    = ('Position', '{%f, %f, %f}' % (img.bbox[:2] + (z,)))

                objKv     = dictType([ ('Texture', os.path.basename(img.fileName)), position , ('Graphic', objName)])

                objKv.update(self.objectDefaults)
                objKv.update(img.orxParams)

                objObj = OrxConfigSection(objName, objKv)

                configFile.write(str(objObj) + OrxConfigSection.endLine)
                objectNames.append(objName)
                z -= zStep

            bodyName    = 'body.%s' % (baseName, )
            objectName  = 'object.%s' % (baseName, )
            graphicName = 'graphic.%s' % (baseName, )
            metaName    = 'object.map.meta'

            # create default values 
            bodyKv      = dictType([('PartList', ' # '.join(partNames))])
            position    = ('Position', '{%f, %f, 0}' % tuple(upperLeft))
            objectKv    = dictType([('Body', bodyName), ('ChildList', ' # '.join(objectNames)), position])
            metaKv      = dictType([('ObjectName', objectName), ('Width', str(self.size[0])), ('Height', str(self.size[1]))])

            # apply command-line updates
            bodyKv.update(self.bodyDefaults)
            objectKv.update(self.objectDefaults)
            metaKv.update(self.metaDefaults)

            BodyObj     = OrxConfigSection(bodyName, bodyKv)
            mainObj     = OrxConfigSection(objectName, objectKv)
            metaObj     = OrxConfigSection(metaName, metaKv)

            configFile.write(str(BodyObj) + OrxConfigSection.endLine)
            configFile.write(str(mainObj) + OrxConfigSection.endLine)
            configFile.write(str(metaObj) + OrxConfigSection.endLine)


def argPairs(args):
    if len(args) % 2 != 0:
        raise RuntimeError('Arguments are not pairs:', repr(args))
    myArgs = list(args)

    pairs = []
    while myArgs:
        pair = myArgs[:2]
        myArgs = myArgs[2:]
        pairs.append(tuple((x.strip() for x in pair)))

    return pairs

def parseOptions():
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--debug', help='Draw physics debug outlines on map image', action='store_true')
    parser.add_argument('-b', '--bodypart', help = 'Body part default key/value pairs', nargs='+', metavar = 'NAME VALUE')
    parser.add_argument('-B', '--body', help = 'Body override key/value pairs', nargs='+', metavar = 'NAME VALUE')
    parser.add_argument('-o', '--object', help = 'Main object override key/value pairs', nargs='+', metavar = 'NAME VALUE')
    parser.add_argument('-m', '--meta', help = 'Meta object override key/value pairs', nargs='+', metavar = 'NAME VALUE')
    parser.add_argument('-g', '--graphic', help = 'Graphics override default key/value pairs', nargs='+', metavar = 'NAME VALUE')

    parser.add_argument('inFiles', nargs='+', metavar='INFILE')
    parser.add_argument('outDir')

    parser.set_defaults(debug = [], bodypart = [], body = [], meta = [], object = [], graphic = [])

    return parser.parse_args()

def usage():
    print 'Usage: %s <Input File> ... <Output Dir>' % (sys.argv[0], )
    sys.exit(1)

def complain(complaint):
    sys.stderr.write(complaint + '\n')
    sys.exit(1)

def main():
    global options

    print progname
    print 'Copyright:', copyright, author
    print

    options = parseOptions()

    bodyPartDefaults = dictType(argPairs(options.bodypart))
    bodyDefaults     = dictType(argPairs(options.body))
    objectDefaults   = dictType(argPairs(options.object))
    metaDefaults     = dictType(argPairs(options.meta))
    graphicDefaults  = dictType(argPairs(options.graphic))

    for inFile in options.inFiles:

        svgConverter = SvgToMap(
                                    inFile, 
                                    outDir = options.outDir, 
                                    bodyPartDefaults = bodyPartDefaults,
                                    bodyDefaults     = bodyDefaults,
                                    objectDefaults   = objectDefaults,
                                    metaDefaults     = metaDefaults,
                                    graphicDefaults  = graphicDefaults,
                                    debug            = options.debug )

        svgConverter.convert(outDir = options.outDir)

main()
